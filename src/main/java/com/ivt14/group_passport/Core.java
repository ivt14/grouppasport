package com.ivt14.group_passport;

import com.ivt14.group_passport.curator_parents_role.Curator;
import com.ivt14.group_passport.group_institute_user.Group;
import com.ivt14.group_passport.group_institute_user.Institute;
import com.ivt14.group_passport.human.Student;
import com.ivt14.group_passport.routes.*;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import static spark.Spark.*;

/**
 * Created by VladimirKonstantinov
 */
public class Core {
    private HashMap<String, FunctionalRoute> routesMap = new HashMap<>();

    public void addRoute(FunctionalRoute route) {
        String routeString = route.getRoute();
        get(routeString, route::get);
        post(routeString, route::post);
    }

    public void addRoute(FunctionalTemplateRoute route) {
        String routeString = route.getRoute();
        get(routeString, route::get, route.getEngine());
        post(routeString, route::post);
    }

    public void init() {
        port(80);
        staticFiles.location("/template");
        Database.getInstanse(); //TODO vova_cons lazy initialization
        addRoute(new DefaultRoute());
        addRoute(new DropDatabaseRoute());
        addRoute(new StudentAddRoute());
        addRoute(new StudentsGetRoute());
        addRoute(new DocumentAddRoute());
        addRoute(new GroupAddRoute());
        addRoute(new GroupGetRoute());
        addRoute(new LogInRoute());
        addRoute(new CuratorAddRoute());
        addRoute(new CuratorShowRoute());
        addRoute(new InstituteAddRoute());
        addRoute(new InstituteGetRoute());
        addRoute(new GroupSearchRoute ());
        addRoute(new StudentEditRoute());
    }

    public static void main(String[] args) {
        Core core = new Core();
        core.init();
        System.out.println("runned");
        Database db = Database.getInstanse();
//        db.drop();
    }
}
