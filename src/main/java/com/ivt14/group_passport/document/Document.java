package com.ivt14.group_passport.document;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by simple on 11/6/2017.
 */
public class Document implements Serializable {
    private DocumentType type;
    private String series;
    private String number;
    private Date gainDate;

    public Document() {}

    public Document(DocumentType type, String series, String number, Date gainDate) {
        this.type = type;
        this.series = series;
        this.number = number;
        this.gainDate = gainDate;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Date getGainDate() {
        return gainDate;
    }

    public void setGainDate(Date gainDate) {
        this.gainDate = gainDate;
    }

    public DocumentType getType() {
        return type;
    }

    public void setType(DocumentType type) {
        this.type = type;
    }
}
