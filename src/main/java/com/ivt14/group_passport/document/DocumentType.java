package com.ivt14.group_passport.document;

/**
 * Created by simple on 11/6/2017.
 */
public enum DocumentType {
    INN, SNILS, PASSPORT
}
