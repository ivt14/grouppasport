package com.ivt14.group_passport.education_fs_ms;

import java.io.Serializable;

public class MartialStatus implements Serializable {
	private String value;

	public MartialStatus(){}

	public MartialStatus(String value){ this.value=value;}
	
	public String getValue(){
		return value;
	}
	
	public void setValue(String value){
		this.value=value;
	}
}