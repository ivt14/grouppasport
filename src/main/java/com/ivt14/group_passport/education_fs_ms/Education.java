package com.ivt14.group_passport.education_fs_ms;

import java.io.Serializable;
import java.util.Date;

public class Education implements Serializable{
	private String value;
	private Date gainDate;

	public Education(){}

	public Education(String value, Date gainDate){
		this.value=value;
		this.gainDate=gainDate;
	}
	
	public String getValue(){
		return value;
	}
	
	public void setValue(String value){
		this.value=value;
	}
	
	public Date getGainDate() {
        return gainDate;
    }

    public void setGainDate(Date gainDate) {
        this.gainDate = gainDate;
    }
}