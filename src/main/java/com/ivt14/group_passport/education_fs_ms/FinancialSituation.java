package com.ivt14.group_passport.education_fs_ms;

public class FinancialSituation{
	private String value;
	
	public FinancialSituation(String value){
		this.value=value;
	}
	
	public String getValue(){
		return value;
	}
	
	public void setValue(String value){
		this.value=value;
	}
}