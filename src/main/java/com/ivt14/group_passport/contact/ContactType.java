package com.ivt14.group_passport.contact;

/**
 * Created by simple on 11/6/2017.
 */
public enum ContactType {
    PHONE,
    EMAIL
}
