package com.ivt14.group_passport.contact;

import java.io.Serializable;

/**
 * Created by simple on 11/6/2017.
 */
public class Contact implements Serializable {
    private ContactType type;
    private String value;

    public Contact() {}

    public Contact(ContactType type, String value) {
        this.type = type;
        this.value = value;
    }

    public ContactType getType() {
        return type;
    }

    public void setType(ContactType type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
