package com.ivt14.group_passport.routes;

import com.ivt14.group_passport.Database;
import com.ivt14.group_passport.curator_parents_role.Curator;
import com.ivt14.group_passport.group_institute_user.User;
import com.ivt14.group_passport.human.Student;
import spark.Request;
import spark.Response;

import java.util.List;

/**
 * Created by Student on 20.11.2017.
 */
public class LogInRoute extends FunctionalRoute {
    public LogInRoute() {
        super("/log/in");
    }

    @Override
    public Object get(Request request, Response response) throws Exception {
        response.redirect("../log_in.html");
        return null;
    }

    @Override
    public Object post(Request request, Response response) throws Exception {
        Database database = Database.getInstanse();
        try {
            String login = request.queryParams("login");
            String password = request.queryParams("password");
            List<Student> students = database.getStudents();
            List<Curator> curators = database.getCurators();
            for (Student student : students) {
                if (student != null && student.getLogin().equals(login))
                    if (student.getPassword().equals(password)) {
                        return "Добро пожаловать, студент " + student.getName();
                }
            }
            for (Curator curator : curators) {
                if (curator != null && curator.getLogin().equals(login)) {
                    if (curator.getPassword().equals(password))
                        return "Добро пожаловать, куратор " + curator.getName();
                }
            }
            response.redirect("../log_in.html");
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }
}
