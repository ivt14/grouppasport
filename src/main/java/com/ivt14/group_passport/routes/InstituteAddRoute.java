package com.ivt14.group_passport.routes;

import com.ivt14.group_passport.Database;
import com.ivt14.group_passport.group_institute_user.Institute;
import spark.Request;
import spark.Response;

import java.util.List;

public class InstituteAddRoute extends FunctionalRoute{
    public InstituteAddRoute(){ super("/institute/add"); }

    @Override
    public Object get(Request request, Response response) throws Exception {
        response.redirect("../add_institute.html");
        return null;
    }

    @Override
    public Object post(Request request, Response response) throws Exception {
        Database database = Database.getInstanse();
        try {
            String instituteName = request.queryParams("name");
            List<Institute> institutes = database.getInstitutes();
            for (Institute institute : institutes) {
                if (institute != null) {
                    if (institute.getInstituteName().equals(instituteName))
                        return "Ошибка, такой институт существует";
                }
            }
            database.add(new Institute(instituteName));
            return "Институт \"" + instituteName + "\" успешно создан!";
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }
}
