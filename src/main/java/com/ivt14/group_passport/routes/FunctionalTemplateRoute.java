package com.ivt14.group_passport.routes;

import spark.*;

import java.io.UnsupportedEncodingException;

/**
 * Created by simple on 11/20/2017.
 */
public class FunctionalTemplateRoute  {
    private final String route;

    public FunctionalTemplateRoute(String route) {
        this.route = route;
    }

    public String getRoute() {
        return route;
    }

    public ModelAndView get(Request request, Response response) throws UnsupportedEncodingException, Exception {
        return null;
    }
    public Object post(Request request, Response response) throws Exception {
        return null;
    }

    public TemplateEngine getEngine() {
        return null;
    }
}
