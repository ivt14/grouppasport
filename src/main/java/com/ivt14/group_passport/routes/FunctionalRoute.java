package com.ivt14.group_passport.routes;

import spark.*;

/**
 * Created by Student on 20.11.2017.
 */
public abstract class FunctionalRoute {
    private final String route;

    public FunctionalRoute(String route) {
        this.route = route;
    }

    public String getRoute() {
        return route;
    }

    public Object get(Request request, Response response) throws Exception{
        return "ERROR: get route " + route + " not set";
    }
    public Object post(Request request, Response response) throws Exception{
        return "ERROR: post route " + route + " not set";
    }
}
