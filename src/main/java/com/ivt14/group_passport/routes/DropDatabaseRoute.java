package com.ivt14.group_passport.routes;

import com.ivt14.group_passport.Database;
import spark.Request;
import spark.Response;

/**
 * Created by Student on 20.11.2017.
 */
public class DropDatabaseRoute extends FunctionalRoute {
    public DropDatabaseRoute() {
        super("db/drop");
    }

    @Override
    public Object get(Request request, Response response) throws Exception {
        Database.getInstanse().drop();
        return "База данных успешно сброшена!";
    }
}
