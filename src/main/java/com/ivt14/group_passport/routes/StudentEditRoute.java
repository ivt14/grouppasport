package com.ivt14.group_passport.routes;

import com.ivt14.group_passport.Database;
import com.ivt14.group_passport.document.Document;
import com.ivt14.group_passport.document.DocumentType;
import com.ivt14.group_passport.human.Student;
import com.ivt14.group_passport.report_hobby_nation.Hobby;
import com.ivt14.group_passport.report_hobby_nation.Nation;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateEngine;
import spark.template.velocity.VelocityTemplateEngine;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Student on 18.12.2017.
 */
public class StudentEditRoute extends FunctionalTemplateRoute {
    public StudentEditRoute() {
        super("student/edit/:student_login");
    }

    @Override
    public ModelAndView get(Request request, Response response) throws UnsupportedEncodingException, Exception {
        try {
            DateFormat df = new SimpleDateFormat("DD/MM/YY", Locale.ENGLISH);
            Map<String, Object> model = new HashMap<>();
            String login = request.params("student_login");
            model.put("login", login);
            Student student = getStudent(login);
            if (student == null) {
                model.put("message", "Студент с таким логином не найден: " + login);
                return new ModelAndView(model, "template/error.html");
            }
            model.put("surname", student.getSurname());
            model.put("name", student.getName());
            model.put("lastname", student.getLastname());
            if (student.getGroup() != null)
                model.put("group", student.getGroup().getGroupName());
            else
                model.put("group", "");
            model.put("institute", "");
            model.put("place", student.getPlace());
            model.put("birthDate", df.format(student.getBirthDate()));
            model.put("birthPlace", student.getBirthPlace());
            model.put("semPol", student.getSemPol());
            model.put("nation", student.getNation().getNation());
            model.put("education", student.getEducation());
            model.put("materialPol", student.getMaterialPol());
            model.put("hobby", student.getHobby().getHobby());
            model.put("passport_series", student.getPassport().getSeries());
            model.put("passport_series", student.getPassport().getSeries());
            model.put("passport_number", student.getPassport().getNumber());
            model.put("passport_gainDate", df.format(student.getPassport().getGainDate()));
            model.put("inn_series", student.getInn().getNumber());
            model.put("snils_series", student.getSnils().getNumber());
            return new ModelAndView(model, "template/student.html");
        } catch(Exception e){
            Map<String, Object> model = new HashMap<>();
            StringBuilder sb = new StringBuilder();
            PrintStream ps = new PrintStream(new OutputStream() {
                @Override
                public void write(int b) throws IOException {
                    sb.append((char) b);
                }
            });
            e.printStackTrace(ps);
            model.put("message", "Ошибка сервера: " + e.getMessage() + "<br> " + sb.toString());
            return new ModelAndView(model, "template/error.html");
        }
    }

    private Student getStudent(String login){
        List<Student> students = Database.getInstanse().getStudents();
        System.out.println("Students:");
        for(Student student  : students)
            System.out.println("\t" + student.getSurname() +
                    " " + student.getName() +
                    " " + student.getLastname() +
                    " " + student.getLogin());
        for(Student student : students){
            if (student.getLogin() != null){
                if (login.equals(student.getLogin())){
                    return student;
                }
            }
        }
        return null;
    }


    @Override
    public Object post(Request request, Response response) throws UnsupportedEncodingException, Exception {
        DateFormat df = new SimpleDateFormat("DD/MM/YY", Locale.ENGLISH);
        String login = request.params("student_login");
        Student student = getStudent(login);
        if (student == null){
            //TODO not except if
            return "Ошибка: студент с таким логином не найден";
        }
        String surname = request.queryParams("surname");
        String name = request.queryParams("name");
        String lastname = request.queryParams("lastname");
        String group = request.queryParams("group");
        String institute = request.queryParams("institute");
        String place = request.queryParams("place");
        String birthDate = request.queryParams("birthDate");
        String birthPlace = request.queryParams("birthPlace");
        String semPol = request.queryParams("semPol");
        String nation = request.queryParams("nation");
        String education = request.queryParams("education");
        String materialPol = request.queryParams("materialPol");
        String hobby = request.queryParams("hobby");
        String passport_series = request.queryParams("passport_series");
        String passport_number = request.queryParams("passport_number");
        String passport_gainDate = request.queryParams("passport_gainDate");
        String inn_series = request.queryParams("inn_series");
        String snils_serires = request.queryParams("snils_series");

        student.setName(name);
        student.setSurname(surname);
        student.setLastname(lastname);
        student.setPlace(place);
        student.setBirthDate(df.parse(birthDate));
        student.setBirthPlace(birthPlace);
        student.setSemPol(semPol);
        student.setNation(new Nation(nation));
        student.setEducation(education);
        student.setMaterialPol(materialPol);
        student.setHobby(new Hobby(hobby));
        student.setPassport(new Document(DocumentType.PASSPORT,
                passport_number, passport_series, df.parse(passport_gainDate)));
        student.setInn(new Document(DocumentType.INN, "", inn_series, df.parse("01/01/1990")));
        student.setSnils(new Document(DocumentType.SNILS, "", snils_serires, df.parse("01/01/1990")));

        Database.getInstanse().update(student);
        response.redirect("/student/edit/" + login);
        return super.post(request, response);
    }

    @Override
    public TemplateEngine getEngine() {
        return new VelocityTemplateEngine();
    }
}
