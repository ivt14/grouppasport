package com.ivt14.group_passport.routes;

import com.ivt14.group_passport.Database;
import com.ivt14.group_passport.group_institute_user.Group;
import com.ivt14.group_passport.group_institute_user.Institute;
import spark.*;
import spark.template.velocity.VelocityTemplateEngine;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by Student on 20.11.2017.
 */
public class GroupGetRoute extends FunctionalTemplateRoute {
    public GroupGetRoute() {
        super("/group/get");
    }


    @Override
    public ModelAndView get(Request request, Response response) throws UnsupportedEncodingException {
        Database db = Database.getInstanse();
        Map<String, Object> model = new HashMap<>();
        List<Institute> institutes = db.getInstitutes();
        String instParam = request.queryParams("inst");
        String groupParam = request.queryParams("group");
        String instParamUtf8 = "";
        String groupParamUtf8 = "";
        try {
            instParamUtf8 = java.net.URLEncoder.encode(request.queryParams("inst"), "UTF-8");
            groupParamUtf8 = java.net.URLEncoder.encode(request.queryParams("group"), "UTF-8");
        } catch (Exception e) {

        }
        if (instParam != null && !instParam.isEmpty()) {
            System.out.println("Institute: " + instParam);
            institutes = new LinkedList<>();
            for (Institute institute : db.getInstitutes()) {
                if (institute.getInstituteName().equals(request.queryParams("inst"))) {
                    if (groupParam != null && !groupParam.isEmpty()) {
                        response.redirect("/student/get?inst=" + instParamUtf8 + "&group=" + groupParamUtf8);
                        System.out.println("Group: " + groupParam);
                        for (Group group : institute.getGroupList()) {
                            if (group.getGroupName().equals(request.queryParams("group"))) {
                                List<Group> groups = new LinkedList<>();
                                groups.add(group);
                                institute.setGroupList(groups);
                                break;
                            }
                        }
                    }
                    institutes.add(institute);
                    break;
                }
            }
        }
        model.put("institutes", institutes);
        return new ModelAndView(model, "template/group_view.html");
    }

    @Override
    public TemplateEngine getEngine() {
        return new VelocityTemplateEngine();
    }
}
