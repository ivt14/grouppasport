package com.ivt14.group_passport.routes;

import spark.Request;
import spark.Response;

/**
 * Created by Maxim on 11/27/2017.
 */
public class DefaultRoute extends FunctionalRoute {
    public DefaultRoute() { super("/"); }

    @Override
    public Object get(Request request, Response response) throws Exception {
        response.redirect("log_in.html");
        return null;
    }
}
