package com.ivt14.group_passport.routes;

import com.ivt14.group_passport.Database;
import com.ivt14.group_passport.group_institute_user.Institute;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateEngine;
import spark.template.velocity.VelocityTemplateEngine;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InstituteGetRoute extends FunctionalTemplateRoute {
    public InstituteGetRoute(){ super("/institute/get"); }

    @Override
    public ModelAndView get(Request request, Response response) {
        Database db = Database.getInstanse();
        Map<String, Object> model = new HashMap<>();
        List<Institute>institutes=db.getInstitutes();
        model.put("institutes", institutes);
        return new ModelAndView(model, "template/institute_view.html");
    }

    @Override
    public TemplateEngine getEngine() {
        return new VelocityTemplateEngine();
    }
}
