package com.ivt14.group_passport.routes;

import com.ivt14.group_passport.Database;
import com.ivt14.group_passport.group_institute_user.Group;
import com.ivt14.group_passport.group_institute_user.Institute;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateEngine;
import spark.template.velocity.VelocityTemplateEngine;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class GroupSearchRoute extends FunctionalTemplateRoute {
    public GroupSearchRoute() {
        super("/group/search");
    }


    @Override
    public ModelAndView get(Request request, Response response) throws UnsupportedEncodingException {
        String groupsearch = request.queryParams("param");
        Database db = Database.getInstanse();
        Map<String, Object> model = new HashMap<>();
        List<Institute> institutes1;
        if (groupsearch!=null && !groupsearch.isEmpty()) {
            institutes1 = new LinkedList<>();
            for (Institute institute : db.getInstitutes()) {
                Institute ins = new Institute(institute.getInstituteName());
                List<Group> groups = new LinkedList<>();
                for (Group group : institute.getGroupList()) {

                    if (group.getGroupName().contains(request.queryParams("param"))) {
                        groups.add(group);

                    }

                }
                if (!groups.isEmpty()) {
                    ins.setGroupList(groups);
                    institutes1.add(ins);
                }

            }
        }
        else{
            institutes1 = db.getInstitutes();
        }
        model.put("institutes", institutes1);
        return new ModelAndView(model, "template/group_search.html");
    }



    @Override
    public TemplateEngine getEngine() {
        return new VelocityTemplateEngine();
    }
}
