package com.ivt14.group_passport.routes;

import com.ivt14.group_passport.Database;
import com.ivt14.group_passport.document.Document;
import com.ivt14.group_passport.document.DocumentType;
import spark.Request;
import spark.Response;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Student on 20.11.2017.
 */
public class DocumentAddRoute extends FunctionalRoute {
    public DocumentAddRoute() {
        super("/document/add/:type/:series/:number/:date");
    }

    @Override
    public Object get(Request req, Response res) throws Exception {
        try {
            DocumentType type = null;
            DateFormat df = new SimpleDateFormat("MM:DD:YY", Locale.ENGLISH);
            Date date = df.parse(req.params("date"));
            if (req.params("type").equals("passport")) {
                type = DocumentType.PASSPORT;
            }
            Document document = new Document(type, req.params("series"), req.params("number"), date);
            //Database.getInstanse().add(document, Document.class);
            //TODO fix it
            return "OK";
        } catch (Exception e) {
            return e.getMessage();
        }
    }
}
