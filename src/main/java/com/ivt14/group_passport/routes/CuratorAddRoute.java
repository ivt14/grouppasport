package com.ivt14.group_passport.routes;

import com.ivt14.group_passport.Database;
import com.ivt14.group_passport.curator_parents_role.Curator;
import spark.Request;
import spark.Response;

import java.util.List;

/**
 * Created by Student on 20.11.2017.
 */
public class CuratorAddRoute extends FunctionalRoute {
    public CuratorAddRoute() {
        super("/curator/add");
    }

    @Override
    public Object get(Request request, Response response) throws Exception {
        response.redirect("../add_curator.html");
        return  null;
    }

    @Override
    public Object post(Request request, Response response) throws Exception {
        Database database = Database.getInstanse();
        try{
            String name=request.queryParams("name");
            String surname=request.queryParams("surname");
            String lastname=request.queryParams("lastname");
            String groupId = request.queryParams("group");
            String institute = request.queryParams("institute");
            List<Curator> curators = database.getCurators();
            for(Curator curator : curators){
                if(curator!=null){
                    if(curator.getName().equals(name) && curator.getSurname().equals(surname)&& curator.getLastname().equals(lastname))
                        return  "Ошибка, такой куратор уже существует";
                }
            }
            Curator curator = new Curator(name,surname,lastname);
            boolean isAdded = database.add(curator, groupId, institute);
            if (isAdded) {
                System.out.println(curator.getLogin());
                System.out.println(curator.getPassword());
                return "Куратор " + surname + " " + name + " " + lastname + " успешно создан!";
            }
            else
                return "Куратор " + surname+" "+name+" "+lastname+" не добавлен!";
        }
        catch (Exception e){
            e.printStackTrace();
            return e.getMessage();
        }
    }
}
