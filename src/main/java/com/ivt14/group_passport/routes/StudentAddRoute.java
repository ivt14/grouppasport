package com.ivt14.group_passport.routes;

import com.ivt14.group_passport.Database;
import com.ivt14.group_passport.group_institute_user.Group;
import com.ivt14.group_passport.group_institute_user.Institute;
import com.ivt14.group_passport.human.Student;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateEngine;
import spark.template.velocity.VelocityTemplateEngine;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Student on 20.11.2017.
 */
public class StudentAddRoute extends FunctionalTemplateRoute {
    public StudentAddRoute() {
        super("/student/add");
    }


    @Override
    public ModelAndView get(Request request, Response response) throws Exception {
        try {
            Database database = Database.getInstanse();
            List<Institute> institutes = database.getInstitutes();
            List<Group> groups = database.getGroups();
            Map<String, Object> model = new HashMap<>();
            model.put("institutes", institutes);
            model.put("groups", groups);
            return new ModelAndView(model, "template/add_student.html");
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public Object post(Request request, Response response) throws Exception {
        try {
            String username = request.queryParams("name");
            String surname = request.queryParams("surname");
            String lastname = request.queryParams("lastname");
            String group = request.queryParams("group");
            String institute = request.queryParams("institute");
            Student student = new Student(username, surname, lastname);
            boolean isAdded = Database.getInstanse().add(student, group, institute);
            if (isAdded) {
                System.out.println(student.getLogin());
                System.out.println(student.getPassword());
                return "Student success added: " + username + surname;
            }
            else
                return "Студент " + student.getName() + " " + student.getSurname() + " не добавлен";
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    @Override
    public TemplateEngine getEngine() {
        return new VelocityTemplateEngine();
    }
}
