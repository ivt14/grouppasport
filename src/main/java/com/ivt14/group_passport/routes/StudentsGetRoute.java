package com.ivt14.group_passport.routes;

import com.ivt14.group_passport.Database;
import com.ivt14.group_passport.group_institute_user.Group;
import com.ivt14.group_passport.group_institute_user.Institute;
import com.ivt14.group_passport.human.Student;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateEngine;
import spark.template.velocity.VelocityTemplateEngine;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Student on 20.11.2017.
 */
public class StudentsGetRoute extends FunctionalTemplateRoute {
    public StudentsGetRoute() {
        super("/student/get");
    }

    @Override
    public ModelAndView get(Request request, Response response) {
        Map<String, Object> model = new HashMap<>();
        String inst = request.queryParams("inst");
        String groupName = request.queryParams("group");
        System.out.println("Institute: " + inst + " group=" + groupName);
        List<Institute> institutes = Database.getInstanse().getInstitutes();
        List<Student> students = null;
        for(Institute institute : institutes){
            if (institute.getInstituteName().equals(inst)){
                for(Group group : institute.getGroupList()){
                    if (group.getGroupName().equals(groupName))
                        students = group.getStudents();
                }
            }
        }
        if (students == null){
            model.put("message", "Группа с таким именем в данном институте не найдена");
            return new ModelAndView(model, "template/error.html");
        }
        model.put("students", students);
        model.put("group_name", groupName);
        model.put("student_add_route", "/student/add");
        return new ModelAndView(model, "template/students_list.html");
    }

    @Override
    public TemplateEngine getEngine() {
        return new VelocityTemplateEngine();
    }
}
