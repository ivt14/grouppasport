package com.ivt14.group_passport.routes;

import com.ivt14.group_passport.Database;
import com.ivt14.group_passport.curator_parents_role.Curator;
import com.ivt14.group_passport.group_institute_user.Group;
import com.ivt14.group_passport.group_institute_user.Institute;
import spark.Request;
import spark.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import spark.*;
import spark.template.velocity.VelocityTemplateEngine;

public class CuratorShowRoute extends FunctionalTemplateRoute {
    public CuratorShowRoute() {
        super("/curator/show");
    }
    @Override
    /*public Object get(Request request, Response response) throws Exception {
        List<Curator> curators = Database.getInstanse().get(Curator.class);
        StringBuilder cu = new StringBuilder();
        cu.append("<h1>Кураторы:</h1>");
        for (Curator curator : curators) {
            cu.append("<p>" + curator.getName() + " "+ curator.getSurname() +" "+ curator.getLastname() + "</p>");
        }
        return cu.toString();}*/
    public ModelAndView get(Request request, Response response) {
        Database db = Database.getInstanse();
        Map<String, Object> model = new HashMap<>();
        List<Curator> curators = db.getCurators();
        List<Group> groups =db.getGroups();
        List<Institute> institutes =db.getInstitutes();
        model.put("curators", curators);
        model.put("groups", groups);
        model.put("institutes", institutes);
        return new ModelAndView(model, "template/List_curator.html");
    }

    @Override
    public TemplateEngine getEngine() {
        return new VelocityTemplateEngine();
    }
}
