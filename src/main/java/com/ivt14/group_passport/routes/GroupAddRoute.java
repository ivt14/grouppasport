package com.ivt14.group_passport.routes;

import com.ivt14.group_passport.Database;
import com.ivt14.group_passport.curator_parents_role.Curator;
import com.ivt14.group_passport.group_institute_user.Group;
import com.ivt14.group_passport.group_institute_user.Institute;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateEngine;
import spark.template.velocity.VelocityTemplateEngine;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created by Student on 20.11.2017.
 */
public class GroupAddRoute extends FunctionalTemplateRoute {
    public GroupAddRoute() {
        super("/group/add");
    }

   /* @Override
    public Object get(Request request, Response response) throws Exception {
        response.redirect("../add_group.html");
        return null;
    }*/
    @Override
    public ModelAndView get(Request request, Response response){
        try {
            Database db=Database.getInstanse();
            Map<String,Object>model=new HashMap<>();
            List<Institute> institutes=db.getInstitutes();
            List<Curator> curators=db.getCurators();
            model.put("institutes", institutes);
            model.put("curators", curators);
            return new ModelAndView(model,"template/add_group.html");
        }
        catch (Exception e){
            System.out.println("FUCK!");
            return null;
        }
    }

    @Override
    public TemplateEngine getEngine() {
        return new VelocityTemplateEngine();
    }

    @Override
    public Object post(Request request, Response response) throws Exception {
        Database database = Database.getInstanse();
        try {
            String groupName = request.queryParams("name");
            String institute = request.queryParams("institute");
            String curator = request.queryParams("curator");

            String curatorName, curatorSurname, curatorLastname;
            if (curator != null && !curator.equals("")) {
                String[] tokens = curator.split(" ");
                curatorName=tokens[0];
                curatorSurname=tokens[1];
                curatorLastname=tokens[2];
            } else {
                curatorName = "";
                curatorLastname = "";
                curatorSurname = "";
            }

            List<Curator> curators=database.getCurators();
            List<Institute> institutes = database.getInstitutes();

            boolean isInstituteExists = false;
            for (Institute institute1 : institutes) {
                if (institute1.getInstituteName().equals(institute)){
                    isInstituteExists = true;
                    for(Group group : institute1.getGroupList()){
                        if (group.getGroupName().equals(groupName))
                            return "Ошибка, такая группа существует";
                    }
                }
            }

            Curator curator2=null;
            for(Curator curator1:curators){
                if(curator1 == null)
                    continue;
                if(curator1.getName().equals(curatorName)&&curator1.getSurname().equals(curatorSurname)&&curator1.getLastname().equals(curatorLastname))
                    curator2=curator1;
              }
            if (isInstituteExists) {
                if (curator2 != null) {
                    Group g = new Group(groupName);
                    g.setCurator(curator2);
                    database.add(g, institute);
                } else {
                    Group g=new Group(groupName);
                    database.add(g,institute);
                }
                return "Группа \"" + groupName + "\" успешно создана в институте " + institute + "!";
            } else {
                return "Группа \"" + groupName + "\" не создана, институт " + institute + " не найден";
            }
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }
}
