package com.ivt14.group_passport.group_institute_user;

import com.ivt14.group_passport.human.Human;
import java.util.Random;
import java.io.Serializable;

public class User extends Human implements Serializable{
    protected String login;
    protected String email;
    protected String password;
    private int type=0;



    public User(){}
    public User(String login, String password){
        this.login=login;
        this.password=password;
    }

    public String getLogin() {
        return login;
    }

    public String getEmail() {
        return email;
    }
    public String getPassword(){
        return password;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String generateLogin(){
        String str = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        String randomLogin = "";
        int index = 0;
        for (int i=0; i<7; i++)
        {
            index = new Random().nextInt(62);
            randomLogin += str.charAt(index);
        }
        return randomLogin;
    }

    public String generatePassword(){
        String str = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        String randomPassword = "";
        int index = 0;
        for (int i=0; i<7; i++)
        {
            index = new Random().nextInt(62);
            randomPassword += str.charAt(index);
        }
        return randomPassword;
    }
}
