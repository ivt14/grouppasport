package com.ivt14.group_passport.group_institute_user;

import com.ivt14.group_passport.curator_parents_role.Curator;
import com.ivt14.group_passport.human.Student;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class Group implements Serializable {

    private String groupName;
    private List<Student> students = new LinkedList<>();

    private Curator curator;

    public Curator getCurator() {
        return curator;
    }

    public void setCurator(Curator curator) {
        this.curator = curator;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void addStudent(Student student) {
        this.students.add(student);
    }

    public Group() {
    }

    public Group(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }
}
