package com.ivt14.group_passport.group_institute_user;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Institute implements Serializable {
    private String instituteName;

    private List<Group> groupList = new ArrayList<>();

    public Institute() {
    }

    public Institute(String instituteName) {
        this.instituteName = instituteName;
    }

    public String getInstituteName() {
        return instituteName;
    }

    public void setInstituteName(String instituteName) {
        this.instituteName = instituteName;
    }

    public List<Group> getGroupList() {
        return groupList;
    }

    public void setGroupList(List<Group> groupList) {
        this.groupList = groupList;
    }
}
