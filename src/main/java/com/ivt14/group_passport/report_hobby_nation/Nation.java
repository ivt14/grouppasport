package com.ivt14.group_passport.report_hobby_nation;

import java.io.Serializable;

public class Nation implements Serializable {
    private String nation;

    public Nation(){}

    public Nation(String value){
        this.nation = value;
    }

    public String getNation(){
        return  nation;
    }

    public void getNation(String value){
        this.nation=value;
    }
}
