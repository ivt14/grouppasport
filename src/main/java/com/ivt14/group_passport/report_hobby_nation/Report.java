package com.ivt14.group_passport.report_hobby_nation;

import java.io.Serializable;
import java.util.Date;

public class Report implements Serializable{
    private String name;
    private Date date;

    public Report(){}

    public Report(String name, Date date){
        this.name=name;
        this.date=date;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name=name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
