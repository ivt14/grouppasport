package com.ivt14.group_passport.report_hobby_nation;

import java.io.Serializable;

public class Hobby implements Serializable {
    private String hobby;

    public Hobby() {
    }

    public Hobby(String value) {
        this.hobby = value;
    }

    public String getHobby() {
        return hobby;
    }

    public void setHobby(String value) {
        this.hobby = value;
    }
}
