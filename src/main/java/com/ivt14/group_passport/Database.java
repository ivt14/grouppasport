package com.ivt14.group_passport;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ivt14.group_passport.curator_parents_role.Curator;
import com.ivt14.group_passport.group_institute_user.Group;
import com.ivt14.group_passport.group_institute_user.Institute;
import com.ivt14.group_passport.group_institute_user.User;
import com.ivt14.group_passport.human.Student;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by VladimirKonstantinov on 06.11.2017.
 */
public class Database {
    private static Database instanse = null;

    private MongoClient mongoClient;
    private MongoDatabase database;
    private HashMap<Class, MongoCollection<Document>> collectionsMap = new HashMap<>();
    public static String DATABASE_NAME = "grouppassport";

    public static synchronized Database getInstanse() {
        if (instanse == null)
            instanse = new Database();
        return instanse;
    }

    private Database() {
        mongoClient = new MongoClient(new MongoClientURI("mongodb://localhost:27017"));
        database = mongoClient.getDatabase(DATABASE_NAME);
    }

    private MongoCollection<Document> getCollection(Class type){
        if (!collectionsMap.containsKey(type)){
            collectionsMap.put(type, database.getCollection(type.getName()));
        }
        return collectionsMap.get(type);
    }

    public String toJson(Object obj, Class<?> type){
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .create();
        return gson.toJson(obj);
    }

    public void add(Institute institute){
        String json = toJson(institute, Institute.class);
        Document document = new Document();
        document.put("value", json);
        getCollection(Institute.class).insertOne(document);
    }

    public boolean add(Group group, String instituteId) {
        List<Institute> institutes = getInstitutes();
        getCollection(Institute.class).drop();
        boolean result = false;
        for(Institute institute : institutes) {
            if (institute.getInstituteName().equals(instituteId)){
                institute.getGroupList().add(group);
                result = true;
                break;
            }
        }
        for(Institute institute : institutes){
            add(institute);
        }
        return result;
    }

    public boolean add(Student student, String groupId, String instituteId) {
        List<Institute> institutes = getInstitutes();
        getCollection(Institute.class).drop();
        boolean result = false;
        for(Institute institute : institutes) {
            if (institute.getInstituteName().equals(instituteId)){
                for(Group group : institute.getGroupList()){
                    if (group.getGroupName().equals(groupId)){
                        group.getStudents().add(student);
                        result = true;
                        break;
                    }
                }
            }
        }
        System.out.println("added student " + student.getSurname() + " " + student.getName());
        for(Institute institute : institutes){
            add(institute);
        }
        return result;
    }

    public void update(Student updatedStudent) {
        List<Institute> institutes = getInstitutes();
        getCollection(Institute.class).drop();
        updatedStudent(updatedStudent, institutes);
        for(Institute institute : institutes){
            add(institute);
        }
    }

    private void updatedStudent(Student updatedStudent, List<Institute> institutes) {
        String exceptedLogin = updatedStudent.getLogin();
        for(Institute institute : institutes) {
            for(Group group : institute.getGroupList()){
                Student foundedStudent = null;
                for(Student student : group.getStudents()) {
                    if (exceptedLogin.equals(student.getLogin())){
                        foundedStudent = student;
                        break;
                    }
                }
                if (foundedStudent != null){
                    List<Student> students = group.getStudents();
                    students.remove(foundedStudent);
                    students.add(updatedStudent);
                    return;
                }
            }
        }
    }

    public boolean add(Curator curator, String groupId, String instituteId) {
        List<Institute> institutes = getInstitutes();
        getCollection(Institute.class).drop();
        boolean result = false;
        for(Institute institute : institutes) {
            if (institute.getInstituteName().equals(instituteId)){
                for(Group group : institute.getGroupList()){
                    if (group.getGroupName().equals(groupId)){
                        group.setCurator(curator);
                        result = true;
                        break;
                    }
                }
            }
        }
        for(Institute institute : institutes){
            add(institute);
        }
        return result;
    }

    public boolean add(User user) {
        String json = toJson(user, User.class);
        Document document = new Document();
        document.put("value", json);
        getCollection(User.class).insertOne(document);
        return true;
    }


    public List<Institute> getInstitutes() {
        List<Institute> result = new LinkedList<>();
        for (Document document : getCollection(Institute.class).find()) {
            String json = (String) document.get("value");
            Gson gson = new GsonBuilder()
                    .setPrettyPrinting()
                    .create();
            result.add(gson.fromJson(json, Institute.class));
        }
        return result;
    }

    public List<Group> getGroups() {
        List<Institute> institutes = getInstitutes();
        List<Group> result = new LinkedList<>();
        for(Institute institute : institutes)
            result.addAll(institute.getGroupList());
        return result;
    }

    public List<Student> getStudents() {
        List<Institute> institutes = getInstitutes();
        List<Student> result = new LinkedList<>();
        for(Institute institute : institutes) {
            for (Group group : institute.getGroupList()) {
                result.addAll(group.getStudents());
            }
        }
        return result;
    }

    public List<Curator> getCurators() {
        List<Institute> institutes = getInstitutes();
        List<Curator> result = new LinkedList<>();
        for(Institute institute : institutes) {
            for (Group group : institute.getGroupList()) {
                if (group.getCurator() == null)
                    continue;
                result.add(group.getCurator());
            }
        }
        return result;
    }

    public List<User> getUsers() {
        List<User> result = new LinkedList<>();
        for (Document document : getCollection(User.class).find()) {
            String json = (String) document.get("value");
            Gson gson = new GsonBuilder()
                    .setPrettyPrinting()
                    .create();
            result.add(gson.fromJson(json, User.class));
        }
        return result;
    }

    public void drop() {
        database.drop();
        database = mongoClient.getDatabase(DATABASE_NAME);
    }
}
