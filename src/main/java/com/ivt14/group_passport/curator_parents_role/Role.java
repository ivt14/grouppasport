package com.ivt14.group_passport.curator_parents_role;

import java.io.Serializable;

public class Role implements Serializable {
    private String parentsRole;

    public Role(){}

    public Role(String parentsRole){
        this.parentsRole=parentsRole;
    }

    public String getparentsRole(){
        return  parentsRole;
    }

    public void setparentsRole(String parentsRole){this.parentsRole=parentsRole;}
}