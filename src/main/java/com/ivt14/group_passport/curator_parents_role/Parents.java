package com.ivt14.group_passport.curator_parents_role;

import com.ivt14.group_passport.human.Human;

import java.io.Serializable;


public class Parents extends Human implements Serializable{
    private String fullName;
    private Role r;
    private String job;

    public Parents(){}

    public Parents(String fullName){
        this.fullName=fullName;
    }

    public String getFullName(){
        return  fullName;
    }

    public Role getRole() {
        return r;
    }

    public String getJob() {
        return job;
    }

    public void setFullName(String fullName){this.fullName=fullName;}

    public void setRole(Role r) {
        this.r = r;
    }

    public void setJob(String job) {
        job = job;
    }
}
