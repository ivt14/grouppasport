package com.ivt14.group_passport.curator_parents_role;

import com.ivt14.group_passport.group_institute_user.Group;
import com.ivt14.group_passport.group_institute_user.User;
import com.ivt14.group_passport.human.Human;

import java.io.Serializable;


public class Curator extends User implements Serializable{
    private User user;
    private Group group;

    public Curator(){}

    public Curator(String name, String surname, String lastname) {
        this.name = name;
        this.surname = surname;
        this.lastname = lastname;
        login = generateLogin();
        password = generatePassword();
    }

    public Curator( User u, Group g){
        this.group=g;
        this.user=u;
    }


    public Group getGroup() {return group;}
    public User getUser() {return user;}

    public void setGroup(Group group) {
        this.group = group;
    }

    public void setUser(User user) {
        this.user = user;
    }
}

