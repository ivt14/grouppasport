package com.ivt14.group_passport.human;


import com.ivt14.group_passport.group_institute_user.User;

import java.io.Serializable;

/**
 * Created by Student on 06.11.2017.
 */
public class DeputyDirector extends Human implements Serializable {
    private User user;

    public DeputyDirector() {
    }

    public DeputyDirector(String name, String surname, String lastname) {
        super(name, surname, lastname);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
