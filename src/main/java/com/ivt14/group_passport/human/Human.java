package com.ivt14.group_passport.human;

import com.ivt14.group_passport.contact.Contact;
import com.ivt14.group_passport.document.Document;

import java.io.Serializable;
import java.util.LinkedList;

/**
 * Created by Student on 06.11.2017.
 */
public class Human implements Serializable {
    protected String surname;
    protected String name;
    protected String lastname;
    private LinkedList<Document> documents;
    private LinkedList<Contact> contacts;

    public Human() {}

    public Human(String name, String surname, String lastname) {
        this.name = name;
        this.surname = surname;
        this.lastname = lastname;
    }

    public void addDocument(Document document) { documents.add(document); }

    public void addContact(Contact contact) { contacts.add(contact); }

    public LinkedList<Document> getDocuments() { return documents; }

    public LinkedList<Contact> getContacts() { return contacts; }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setDocuments(LinkedList<Document> documents) {
        this.documents = documents;
    }

    public void setContacts(LinkedList<Contact> contacts) {
        this.contacts = contacts;
    }
}
