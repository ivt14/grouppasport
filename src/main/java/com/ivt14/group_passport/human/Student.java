package com.ivt14.group_passport.human;

import com.ivt14.group_passport.document.Document;
import com.ivt14.group_passport.document.DocumentType;
import com.ivt14.group_passport.report_hobby_nation.Hobby;
import com.ivt14.group_passport.report_hobby_nation.Nation;
import com.ivt14.group_passport.group_institute_user.Group;
import com.ivt14.group_passport.group_institute_user.User;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Student on 06.11.2017.
 */

public class Student extends User implements Serializable {
    private String place = "";
    private Date birthDate = new Date(System.currentTimeMillis());
    private String birthPlace = "";
    private Group group;
    private String semPol = "";
    private Nation nation = new Nation("");
    private String education = "";
    private String materialPol = "";
    private Hobby hobby = new Hobby("");
    private Document passport;
    private Document inn;
    private Document snils;

    public Student() {
        try {
            DateFormat df = new SimpleDateFormat("DD/MM/YY", Locale.ENGLISH);
            passport = new Document(DocumentType.PASSPORT, "", "", df.parse("01/01/1990"));
            inn = new Document(DocumentType.INN, "", "", df.parse("01/01/1990"));
            snils = new Document(DocumentType.SNILS, "", "", df.parse("01/01/1990"));
        } catch(Exception e){}
    }

    public Student(String name, String surname, String lastname) {
        this.name = name;
        this.surname = surname;
        this.lastname = lastname;
        login = generateLogin();
        password = generatePassword();
        try {
            DateFormat df = new SimpleDateFormat("DD/MM/YY", Locale.ENGLISH);
            passport = new Document(DocumentType.PASSPORT, "", "", df.parse("01/01/1990"));
            inn = new Document(DocumentType.INN, "", "", df.parse("01/01/1990"));
            snils = new Document(DocumentType.SNILS, "", "", df.parse("01/01/1990"));
        } catch(Exception e){}
    }

    public String getPlace() {
        return place;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public Hobby getHobby() {
        return hobby;
    }

    public Group getGroup() {
        return group;
    }

    public String getSemPol() {
        return semPol;
    }

    public Nation getNation() {
        return nation;
    }

    public String getEducation() {
        return education;
    }

    public String getMaterialPol() {
        return materialPol;
    }

    public Document getPassport() {
        return passport;
    }

    public Document getInn() {
        return inn;
    }

    public Document getSnils() {
        return snils;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public void setHobby(Hobby hobby) {
        this.hobby = hobby;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public void setSemPol(String semPol) {
        this.semPol = semPol;
    }

    public void setNation(Nation nation) {
        this.nation = nation;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public void setMaterialPol(String materialPol) {
        this.materialPol = materialPol;
    }

    public void setPassport(Document passport) {
        this.passport = passport;
    }

    public void setInn(Document inn) {
        this.inn = inn;
    }

    public void setSnils(Document snils) {
        this.snils = snils;
    }
}
